#!/usr/bin/env python

from argparse import ArgumentParser, ArgumentTypeError
from os import listdir, mkdir
from os.path import isdir, join, splitext
from random import randint

import numpy as np
from PIL import Image, ImageDraw, ImageFont


def draw_text(img: Image, text: str, size: int=32, color: int=0x000000, size_outline: int=None, color_outline: int=None):
    """Write text in center of image

    Args:
        img (Image): Image
        text (str): Text to write
        size (int, optional): Text size. Defaults to 32.
        color (int, optional): Text color. Defaults to 0x000000.
        size_outline (int, optional): Text outline size. Defaults to None.
        color_outline (int, optional): Text outline color. Defaults to None.

    Returns:
        Image: Image with text
    """    
    font_path = "/home/thbz/.local/share/fonts/ttf/Pokemon Classic/Pokemon Classic.ttf"
    font = ImageFont.truetype(font_path, size=size)
    draw = ImageDraw.Draw(img)
    W, H = img.size
    w, h = draw.textsize(text, font=font)
    x = (W-w)//2
    y = (H-h)//2 - h//5
    
    if size_outline is not None or color_outline is not None:
        if size_outline is None:
            size_outline = size
        if color_outline is None:
            color_outline = 0x000000
            
        font_outline = ImageFont.truetype(font_path, size=size)
        draw.text((x-size_outline, y), text, fill=color_outline, font=font_outline)
        draw.text((x+size_outline, y), text, fill=color_outline, font=font_outline)
        draw.text((x, y-size_outline), text, fill=color_outline, font=font_outline)
        draw.text((x, y+size_outline), text, fill=color_outline, font=font_outline)
    
    draw.text((x, y), text, fill=color, font=font)
    return img

def draw_circle(img: Image, width: int, color: int, width_outline: int, color_outline: int):
    """Draw circle around image, optionally with outline

    Args:
        img (Image): Image
        width (int): Circle width.
        color (int): Circle color.
        width_outline (int): Circle outline width.
        color_outline (int): Circle outline color.

    Returns:
        Image: Image with circle
    """    
    draw = ImageDraw.Draw(img)

    if width_outline or color_outline:
        if width_outline:
            width_outline = width+width_outline
        else:
            width_outline = width+1
        draw.ellipse([(0, 0), img.size], width=width_outline+1, outline=0x000000, fill=None)
        draw.ellipse([(0, 0), img.size], width=width_outline, outline=color_outline, fill=None)

    draw.ellipse([(0, 0), img.size], width=width, outline=color, fill=None)
    return img

def draw_noise(img: Image, width: int=2, height: int=2, darken: int=40):
    """Fill image with random pixels

    Args:
        img (Image): Image
        width (int, optional): Pixels width. Defaults to 2.
        height (int, optional): Pixels height. Defaults to 2.
        darken (int, optional): Decrease each pixel's RGB values. Defaults to 40.

    Returns:
        Image: Image filled with random pixels.
    """   
    draw = ImageDraw.Draw(img)

    for y in range(0, img.size[1], height):
        for x in range(0, img.size[0], width):
            fill = (randint(0,255)-darken, randint(0,255)-darken, randint(0,255)-darken)
            for xx in range(x, x+width):
                for yy in range(y, y+height):
                    draw.point((xx,yy), fill=fill)           
    return img

def crop(img: Image):
    """Circle crop image

    Args:
        img (Image): Image

    Returns:
        Image: Cropped image
    """    
    h, w = img.size
    np_img = np.array(img)
    alpha = Image.new("L", img.size, 0)
    draw = ImageDraw.Draw(alpha)
    draw.pieslice([0,0,h,w], 0, 360, fill=0xff)
    np_alpha = np.array(alpha)
    np_img = np.dstack((np_img, np_alpha))
    return Image.fromarray(np_img)

def save(img: Image, name: str):
    """Save image to file

    Args:
        img (Image): Image
        name (str, optional): Optional filename. Defaults to incremental number.
    """    
    if not isdir("gen"):
        mkdir("gen")

    if not name:
        if existing := [int(splitext(x)[0]) for x in listdir("gen")]:
            index = sorted(existing)[-1] + 1
        else:
            index = 0
        name = str(index)

    img.save(join("gen", name + ".png"))

def color_int(c: str):
    """Converts color string into integer

    Args:
        c (str): Color string like #5b4fff.

    Returns:
        str: Color integer
    """
    rgb = [int(c[i:i+2], 16) for i in range(1,len(c),2)]
    color = rgb[2]*0x10000 + rgb[1]*0x100 + rgb[0]  # why the fuck do i need to do this?
    return color

def parse_size(i: int):
    try:
        size = int(i)
    except:
        raise ArgumentTypeError(f"Invalid argument ({i}), must be an integer.")
    if size < 128:
        raise ArgumentTypeError(f"Invalid argument ({i}), expected value > 128.")
    elif size > 1024:
        raise ArgumentTypeError(f"Invalid argument ({i}), expected value < 1024.")
    return size

def parse_color(c: str):
    try:
        color = int(c.lstrip("#").lstrip("0x"), 16)
    except:
        raise ArgumentTypeError(f"Invalid argument ({c}), expected value must be like #5b4fff.")
    if color < 0x000000:
        raise ArgumentTypeError(f"Invalid argument ({c}), expected value > #000000.")
    elif color > 0xffffff:
        raise ArgumentTypeError(f"Invalid argument ({c}), expected value < #ffffff.")
    return "#" + hex(color)[2:]

def parse_darken(i: int):
    try:
        darken = int(i)
    except:
        raise ArgumentTypeError(f"Invalid argument ({i}), must be an integer.")
    if darken < 0:
        raise ArgumentTypeError(f"Invalid argument ({i}), expected value >=0.")
    elif darken > 255:
        raise ArgumentTypeError(f"Invalid argument ({i}), expected value <=255.")
    return darken

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "name",
        nargs="+",
        help="Avatar(s) name(s)"
    )
    parser.add_argument(
        "--size", "-s",
        type=parse_size,
        default=256,
        help="Image width (=height). Must be >128 and <1024. Defaults to 256."
    )
    parser.add_argument(
        "--color", "-c",
        type=parse_color,
        default="#5B4FFF",
        help="Circle color. Must be >0x000000 and <0xffffff. Defaults to #5B4FFF."
    )
    parser.add_argument(
        "--darken", "-d",
        type=parse_darken,
        default=40,
        help="Decrease each pixel's RGB values. Must be >=0 and <=255. Defaults to 40."
    )
    parser.add_argument(
        "--text", "-t",
        default=None,
        help="Centered text."
    )
    args = parser.parse_args()
    
    size = (args.size, args.size)
    color = color_int(args.color)
    circle_outline_size = 3
    text = args.text
    text_outline_size = 2
    text_outline_color = color_int("#000000")

    for n in args.name:
        img = Image.new("RGB", size)
        img = draw_noise(img, darken=args.darken)
        img = draw_circle(img, width=8, color=0x000000, width_outline=circle_outline_size, color_outline=color)
        if text:
            img = draw_text(img, text, color=color, size_outline=text_outline_size, color_outline=text_outline_color)
        img = crop(img)
    
        save(img, n)
